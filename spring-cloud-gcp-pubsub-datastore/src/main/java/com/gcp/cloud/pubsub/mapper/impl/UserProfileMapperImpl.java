package com.gcp.cloud.pubsub.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.gcp.cloud.pubsub.entity.User;
import com.gcp.cloud.pubsub.mapper.UserProfileMapper;
import com.gcp.cloud.pubsub.model.UserDto;


@Component
public class UserProfileMapperImpl implements UserProfileMapper {

	public List<UserDto> getDTOFromModel(List<User> userDetails) {
		if (userDetails == null) {
			return null;
		}

		List<UserDto> list = new ArrayList<UserDto>(userDetails.size());
		for (User user : userDetails) {
			list.add(getUserDto(user));
		}
		return list;
	}

	private UserDto getUserDto(User user) {
		if (user == null) {
			return null;
		}

		UserDto userDto = new UserDto();
		userDto.setUserId(user.getUserId());
		userDto.setUsername(user.getUsername());
		userDto.setPassword(user.getPassword());
		userDto.setEmail(user.getEmail());
		return userDto;
	}

	@Override
	public User getModelFromDTO(UserDto userDto) {
		if(userDto == null) {
			return null;
		}
		User user = new User();
		user.setID(userDto.getUserId()+"@"+userDto.getUsername());
		user.setUserId(userDto.getUserId());
		user.setUsername(userDto.getUsername());
		user.setPassword(userDto.getPassword());
		user.setEmail(userDto.getEmail());
		return user;
	}

	@Override
	public UserDto getDTOFromModel(User user) {
		if (user == null) {
			return null;
		}

		UserDto userDto = new UserDto();
		userDto.setUserId(user.getUserId());
		userDto.setUsername(user.getUsername());
		userDto.setPassword(user.getPassword());
		userDto.setEmail(user.getEmail());
		return userDto;
		
	}
}
