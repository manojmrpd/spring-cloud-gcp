package com.gcp.cloud.pubsub.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gcp.cloud.pubsub.SpringCloudGcpPubsubDatastoreApplication.PubsubOutboundGateway;
import com.gcp.cloud.pubsub.exception.ErrorApiResponses;
import com.gcp.cloud.pubsub.model.Constants;
import com.gcp.cloud.pubsub.model.ResponsePreparer;
import com.gcp.cloud.pubsub.model.UserDto;
import com.gcp.cloud.pubsub.service.UserProfileService;

@RestController
public class PubSubDataStoreController {

	@Autowired
	private UserProfileService userProfileService;

	@Autowired
	private ResponsePreparer responsePreparer;
	
	@Autowired
	PubsubOutboundGateway messageGateway;

	@ErrorApiResponses
	@GetMapping(value = "/userDetails", produces = "application/json")
	public ResponseEntity<List<UserDto>> getUserDetails(
			@RequestHeader(value = Constants.CORRELATIONID, required = true) String correlationId,
			@RequestHeader(value = Constants.CLIENTID, required = true) String clientId) {

		ResponseEntity<List<UserDto>> response = null;
		List<UserDto> userDetails = userProfileService.getUserDetails();
		if (null != userDetails && !userDetails.isEmpty()) {
			return ResponseEntity.ok().headers(responsePreparer.getResponseHttpHeaders(correlationId, clientId))
					.body(userDetails);
		} else {
			new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return response;
	}
	
	@ErrorApiResponses
	@PostMapping(value = "/createUser", consumes = "application/json")
	public ResponseEntity<UserDto> createUser(@RequestBody(required = true) UserDto userDto, @RequestHeader(value = Constants.CORRELATIONID, required = true) String correlationId,
			@RequestHeader(value = Constants.CLIENTID, required = true) String clientId) {
		
		UserDto userInfo = userProfileService.createUser(userDto);
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(userInfo.getUserId())
				.toUri();
			
		messageGateway.sendToPubsub(userDto.toString());
		return ResponseEntity.created(location).headers(responsePreparer.getResponseHttpHeaders(correlationId, clientId)).body(userInfo);
		
	}

}
