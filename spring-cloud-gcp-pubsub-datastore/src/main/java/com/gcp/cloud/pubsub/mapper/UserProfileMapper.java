package com.gcp.cloud.pubsub.mapper;

import java.util.List;

import org.springframework.stereotype.Component;

import com.gcp.cloud.pubsub.entity.User;
import com.gcp.cloud.pubsub.model.UserDto;



@Component
public interface UserProfileMapper {

	List<UserDto> getDTOFromModel(List<User> userDetails);

	User getModelFromDTO(UserDto userDto);

	UserDto getDTOFromModel(User user);

}
