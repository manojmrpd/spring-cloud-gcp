package com.gcp.cloud.pubsub.exception;

import java.util.ArrayList;
import java.util.List;

public class BusinessException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String code;
	private final ErrorCodeIF errorCode;
	private final List<Error> errors;
	
	public BusinessException(ErrorCodeIF errorCode) {
		super(errorCode.getMessage());
		this.code = errorCode.getCode();
		this.errors = new ArrayList<>();
		this.errors.add(errorCode.getError());
		this.errorCode = errorCode;
	}
	
	public BusinessException(ErrorCodeIF errorCode, List<Error> errors) {
		super(errorCode.getMessage());
		this.code = errorCode.getCode();
		this.errors = errors;
		this.errorCode = errorCode;
	}

	public String getCode() {
		return code;
	}

	public ErrorCodeIF getErrorCode() {
		return errorCode;
	}

	public List<Error> getErrors() {
		return errors;
	}

	@Override
	public String toString() {
		return "BusinessException [code=" + code + ", errorCode=" + errorCode + ", errors=" + errors + "]";
	}

}
