package com.gcp.cloud.pubsub.logging;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

public interface LoggerServiceIF {

	public void logRequest(HttpServletRequest request, String message);
	
	public void logRequest(HttpRequest request, String message);

	public void logResponse(ContentCachingResponseWrapper response, ContentCachingRequestWrapper request, long elapsedTime);

	public void logResponse(ClientHttpResponse response, HttpRequest request, long elapsedTime);

	public void logError(Exception exp, String operation);

	public void logWarn(String message, String operation);

	public void logInfo(String message, String operation);

	public void logDebug(String message, String operation);

}
