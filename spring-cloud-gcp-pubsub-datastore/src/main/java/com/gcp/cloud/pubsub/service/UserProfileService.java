package com.gcp.cloud.pubsub.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gcp.cloud.pubsub.model.UserDto;

@Service
public interface UserProfileService {

	List<UserDto> getUserDetails();

	UserDto createUser(UserDto userDto);

}
