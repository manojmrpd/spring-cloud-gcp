package com.gcp.cloud.pubsub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gcp.cloud.pubsub.SpringCloudGcpPubsubDatastoreApplication.PubsubOutboundGateway;
import com.gcp.cloud.pubsub.model.PubSubMessage;

@RestController
public class PubSubController {
	
	@Autowired
	PubsubOutboundGateway messageGateway;
	
	@RequestMapping(value = "/publish", method = RequestMethod.POST)
	public String publishMessage(@RequestBody PubSubMessage message) {
		messageGateway.sendToPubsub(message.toString());
		return "Message published to Google Pub/Sub successfully";
	}

}
