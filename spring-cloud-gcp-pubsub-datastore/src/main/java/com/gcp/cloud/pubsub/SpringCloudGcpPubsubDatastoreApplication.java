package com.gcp.cloud.pubsub;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import com.google.cloud.spring.data.datastore.repository.config.EnableDatastoreRepositories;
import com.google.cloud.spring.pubsub.core.PubSubOperations;
import com.google.cloud.spring.pubsub.integration.AckMode;
import com.google.cloud.spring.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import com.google.cloud.spring.pubsub.integration.outbound.PubSubMessageHandler;
import com.google.cloud.spring.pubsub.support.BasicAcknowledgeablePubsubMessage;
import com.google.cloud.spring.pubsub.support.GcpPubSubHeaders;

@SpringBootApplication
@EnableDatastoreRepositories
public class SpringCloudGcpPubsubDatastoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudGcpPubsubDatastoreApplication.class, args);
	}
	
	// Create MessageChannel Bean for pubsubInputChannel
		@Bean
		public MessageChannel pubsubInputChannel() {
			return new DirectChannel();
		}

		// Create PubSubInboundChannelAdapter Bean for messageChannelAdapter
		@Bean
		public PubSubInboundChannelAdapter messageChannelAdapter(
				@Qualifier("pubsubInputChannel") MessageChannel inputChannel, PubSubOperations pubSubTemplate) {
			PubSubInboundChannelAdapter adapter = new PubSubInboundChannelAdapter(pubSubTemplate, "testSubscription");
			adapter.setOutputChannel(inputChannel);
			adapter.setAckMode(AckMode.MANUAL);
			return adapter;
		}

		@Bean
		@ServiceActivator(inputChannel = "pubsubInputChannel")
		public MessageHandler messageReceiver() {
		    return message -> {
		       System.out.println("Message arrived! Payload: " + new String((byte[]) message.getPayload()));
		        BasicAcknowledgeablePubsubMessage originalMessage =
		              message.getHeaders().get(GcpPubSubHeaders.ORIGINAL_MESSAGE, BasicAcknowledgeablePubsubMessage.class);
		        originalMessage.ack();
		    };
		}

		// Create Service Activator Bean for Pub sub output channel

		@Bean
		@ServiceActivator(inputChannel = "pubsubOutputChannel")
		public MessageHandler messageSender(PubSubOperations pubsubTemplate) {
			return new PubSubMessageHandler(pubsubTemplate, "testTopic");
		}

		// Create Messaging Gateway interface to publish the message
		@MessagingGateway(defaultRequestChannel = "pubsubOutputChannel")
		public interface PubsubOutboundGateway {
			void sendToPubsub(String text);
		}	
}
