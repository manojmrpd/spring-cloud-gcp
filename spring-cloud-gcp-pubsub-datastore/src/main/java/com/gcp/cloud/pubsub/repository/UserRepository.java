package com.gcp.cloud.pubsub.repository;

import java.util.List;

import com.gcp.cloud.pubsub.entity.User;
import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;
import com.google.cloud.spring.data.datastore.repository.query.Query;

public interface UserRepository extends DatastoreRepository<User, Long> {
	
	@Query("select * from `User`")
	List<User> findAll();
	
	@Query("select * from `User` where userId=@userId")
	List<User> getUserId(String userId);

}
