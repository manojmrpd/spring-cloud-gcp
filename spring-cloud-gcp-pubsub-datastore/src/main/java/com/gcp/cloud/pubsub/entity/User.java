package com.gcp.cloud.pubsub.entity;

import org.springframework.data.annotation.Id;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "User")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User  {
	
	@Id
	private String ID;
	private String userId;
	private String username;
	private String password;
	private String email;
	
	

	
}
