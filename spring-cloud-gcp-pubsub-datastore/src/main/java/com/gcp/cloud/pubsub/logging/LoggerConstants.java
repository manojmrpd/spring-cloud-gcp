package com.gcp.cloud.pubsub.logging;

public class LoggerConstants {
	
	public static final String HTTPCONTENT_JSON = "application/json";
	public static final String APPID_DATA = "APPID_DATA";
	public static final String COMPONENT_DATA = "COMPONENT_DATA";
	public static final String TXN = "TXN";
	public static final String SUBSCRIBER_EVENT = "subscriber_event";
	public static final String EVENT = "event";
	public static final String ERROR_DEFAULT = "ERROR";
	public static final String MSG = "MSG";
	public static final String RESPONSE = "RESPONSE";
	public static final String PUBLISHED = "PUBLISHED";
	public static final String SUBSCRIBED = "SUBSCRIBED";
	public static final String OPERATION_DATA = "OPERATION_DATA";
	public static final String URL_DATA = "URL_DATA";
	public static final String HTTPMETHOD_DATA = "HTTPMETHOD_DATA";
	public static final String CONTENTTYPE_DATA = "CONTENTTYPE_DATA";
	public static final String MESSAGEID_DATA = "MESSAGEID_DATA";
	public static final String TOPIC_DATA = "TOPIC_DATA";
	public static final String SUBSCRIPTION_DATA = "SUBSCRIPTION_DATA";
	public static final String UNKONWN_CONTENT = "unknownContent";

	public static final String REQUESTHEADERS_JSON = "REQUESTHEADERS_JSON";
	public static final String REQUESTTIME_DATA = "REQUESTTIME_DATA";
	public static final String REQUESTBODY_JSON = "REQUESTBODY_JSON";
	public static final String REQUESTQUERYPARAMS_DATA = "REQUESTQUERYPARAMS_DATA";
	public static final String REQUEST = "request";
	public static final String REQUEST_GET = "request_get";

	public static final String INFO = "info";
	public static final String WARN = "warn";
	public static final String DEBUG = "debug";
	public static final String ERROR = "error";
	public static final String ERROR_BUSINESS = "error_business";
	public static final String RESPONSEHEADERS_JSON = "RESPONSEHEADERS_JSON";
	public static final String RESPONSEBODY_JSON = "RESPONSEBODY_JSON";
	public static final String EVENTBODY_JSON = "EVENTBODY_JSON";
	public static final String EVENTHEADERS_JSON = "EVENTHEADERS_JSON";
	public static final String DEBUG_DATA = "DEBUG_DATA";
	public static final String WARN_DATA = "WARN_DATA";
	public static final String INFO_DATA = "INFO_DATA";
	public static final String ERROR_DATA = "ERROR_DATA";
	public static final String ELAPSEDTIME_DATA = "ELAPSEDTIME_DATA";
	public static final String HTTPSTATUSCODE_DATA = "HTTPSTATUSCODE_DATA";
	public static final String HTTPSTATUS_DATA = "HTTPSTATUS_DATA";
	public static final String ERRORCODE_DATA = "ERRORCODE_DATA";
	public static final String ERRORMESSAGE_DATA = "ERRORMESSAGE_DATA";
	public static final String ERRORSTACKTRACE_DATA = "ERRORSTACKTRACE_DATA";
	private LoggerConstants() {
	}
}
