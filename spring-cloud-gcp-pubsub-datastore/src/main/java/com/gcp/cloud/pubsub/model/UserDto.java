package com.gcp.cloud.pubsub.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class UserDto {
	

	private String userId;
	private String username;
	private String password;
	private String email;

}
