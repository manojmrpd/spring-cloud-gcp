package com.gcp.cloud.pubsub.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcp.cloud.pubsub.entity.User;
import com.gcp.cloud.pubsub.exception.BusinessException;
import com.gcp.cloud.pubsub.exception.CommonErrorCodes;
import com.gcp.cloud.pubsub.mapper.UserProfileMapper;
import com.gcp.cloud.pubsub.model.UserDto;
import com.gcp.cloud.pubsub.repository.UserRepository;
import com.gcp.cloud.pubsub.service.UserProfileService;

@Service
public class UserProfileServiceImpl implements UserProfileService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserProfileMapper mapper;

	@Override
	public List<UserDto> getUserDetails() {
		
		List<UserDto> users = new ArrayList<>();
		List<User> userList= userRepository.getUserId("1");
		
		if(null != userList && !userList.isEmpty()) {
			users = mapper.getDTOFromModel(userList);
			return users;
		} else {
			throw new BusinessException(CommonErrorCodes.USER_DATA_NOT_EXIST);
		}
	}

	@Override
	public UserDto createUser(UserDto userDto) {
		User user = mapper.getModelFromDTO(userDto);
		User createdUser = userRepository.save(user);
		return mapper.getDTOFromModel(createdUser);
	}
}
